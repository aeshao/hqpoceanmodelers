# HQPOceanModelers

Resources for graduate students and postdoctoral fellows (referred to as highly-qualified personnel) interested in ocean modeling.
Please contribute to the wiki or post issues about any modeling problems you encounter.

# History
2019: This repository started as a resource to enable communication for HQP who attended an IUGG organized by Birgit Rogalla and Andrew Shao. All are welcome to contribute.